package com.kristijandraca.guidelib

import android.app.Activity
import android.content.Context
import android.graphics.*
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.util.AttributeSet
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.kristijandraca.guidelib.databinding.ViewActionBinding


class GuideView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    FrameLayout(context, attrs, defStyleAttr) {

    private val instance: GuideView = this
    //private var targetRect: RectF? = null
    private var targetRects = ArrayList<RectF>()
    private val targetPaint = Paint(ANTI_ALIAS_FLAG)
    private val X_FER_MODE_CLEAR = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    private val RADIUS_SIZE_TARGET_RECT = 15
    private val selfRect = Rect()
    private val selfPaint = Paint()
    private var activity: Activity? = null
    private var builder: Builder? = null

    constructor(activity: Activity, builder: Builder) : this(activity) {
        this.activity = activity
        this.builder = builder
    }

    init {
        setWillNotDraw(false)
        setLayerType(View.LAYER_TYPE_HARDWARE, null)


        recalculateTargetRect()

        val layoutListener = object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                viewTreeObserver.removeOnGlobalLayoutListener(this)

                recalculateTargetRect()

                selfRect.set(
                    paddingLeft,
                    paddingTop,
                    width - paddingRight,
                    height - paddingBottom
                )

                viewTreeObserver.addOnGlobalLayoutListener(this)
            }
        }
        viewTreeObserver.addOnGlobalLayoutListener(layoutListener)
        invalidate()
    }

    private fun recalculateTargetRect() {
        val statusBarHeight = getStatusBarHeight()

        builder?.targetViews?.let { targets ->

            targetRects.clear()

            for (targetView in targets) {

                val locationTarget = IntArray(2)
                targetView.getLocationOnScreen(locationTarget)
                builder?.let {
                    targetRects.add(
                        RectF(
                            locationTarget[0].toFloat() - it.paddingFocusLeft,
                            locationTarget[1].toFloat() - statusBarHeight - it.paddingFocusTop,
                            (locationTarget[0] + targetView.width).toFloat() + it.paddingFocusRight,
                            (locationTarget[1] + targetView.height).toFloat() - statusBarHeight + it.paddingFocusBottom
                        )
                    )
                }
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?.let { e ->
            builder?.let {
                for (targetRect in targetRects) {
                    if (targetRect.contains(e.x, e.y) && it.isTargetClicable) {
                        return super.onTouchEvent(event)
                    }
                }
            }
        }
        return true
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }


    private fun show() {
        val binding: ViewActionBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_action, this, false)

        builder?.let { builder ->
            binding.messageText = builder.messageText
            binding.buttonText = builder.buttonText
            if (builder.actionClickListener != null) {
                binding.btAction.setOnClickListener { builder.actionClickListener?.onActionClick(instance) }
            } else {
                binding.btAction.visibility = View.INVISIBLE
            }
            if (builder.backgroundColor != -1) {
                binding.card.setCardBackgroundColor(builder.backgroundColor)
            }
            if (builder.messageTextColor != -1) {
                binding.tvMessage.setTextColor(builder.messageTextColor)
            }
            binding.tvMessage.gravity = builder.messageGravity

            binding.executePendingBindings()

            if (builder.isAnimationEnabled) {
                val bottomUp = AnimationUtils.loadAnimation(
                    context,
                    R.anim.bottom_up
                )
                bottomUp.duration = builder.animationDuration
                binding.root.startAnimation(bottomUp)
            }
        }


        val layoutParams = LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        layoutParams.gravity = Gravity.BOTTOM
        builder?.messageText?.let { messageText ->
            if (messageText.isNotEmpty()) {
                addView(binding.root, layoutParams)
            }
        }


        activity?.let {
            getViewRoot(it).addView(this)
        }


    }

    /**
     * Dismiss GuideView
     */
    fun dismiss() {
        activity?.let {
            getViewRoot(it).removeView(this)
        }

    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        selfPaint.color = ContextCompat.getColor(context, R.color.transparent_grey)
        selfPaint.style = Paint.Style.FILL
        selfPaint.isAntiAlias = true
        canvas?.drawRect(selfRect, selfPaint)

        for (targetRect in targetRects) {
            targetPaint.xfermode = X_FER_MODE_CLEAR
            targetPaint.isAntiAlias = true
            canvas?.drawRoundRect(
                targetRect,
                RADIUS_SIZE_TARGET_RECT.toFloat(),
                RADIUS_SIZE_TARGET_RECT.toFloat(),
                targetPaint
            )

        }


    }

    private fun getViewRoot(activity: Activity): ViewGroup {
        val androidContent = activity.findViewById<ViewGroup>(android.R.id.content)
        return androidContent.parent.parent as ViewGroup
    }

    /**
     * GuideView Builder
     * @param activity where GuideView is going to be shown
     */
    class Builder(private var activity: Activity) {

        internal var messageText: String? = null
        internal var messageGravity: Int = Gravity.CENTER
        internal var messageTextColor: Int = -1
        internal var buttonText: String? = null
        internal var actionClickListener: GuideViewListener? = null
        internal var targetViews = ArrayList<View>()
        internal var isTargetClicable: Boolean = false
        internal var backgroundColor: Int = -1
        internal var paddingFocusTop: Float = 8f
        internal var paddingFocusLeft: Float = 8f
        internal var paddingFocusBottom: Float = 8f
        internal var paddingFocusRight: Float = 8f
        internal var isAnimationEnabled: Boolean = true
        internal var animationDuration: Long = 500

        /**
         * Message gravity
         * @param message gravity
         * @return Builder
         */
        fun messageGravity(gravity: Int): Builder {
            this.messageGravity = gravity
            return this
        }

        /**
         * Display message
         * @param message to display
         * @return Builder
         */
        fun message(message: String): Builder {
            this.messageText = message
            return this
        }

        /**
         * Controls button
         * @param buttonText text on button
         * @param actionClickListener instance of GuideViewListener that is triggered on button click
         * @return Builder
         */
        fun action(buttonText: String, actionClickListener: GuideViewListener): Builder {
            this.buttonText = buttonText
            this.actionClickListener = actionClickListener
            return this
        }

        /**
         * Highlight View
         * @param view that needs to be highlighted
         * @return Builder
         */
        fun target(vararg target: View): Builder {
            this.targetViews.addAll(target)
            return this
        }

        /**
         * Is target clickable
         * @param isTargetClickable
         * @return Builder
         */
        fun isTargetClickable(isTargetClickable: Boolean): Builder {
            this.isTargetClicable = isTargetClickable
            return this
        }

        /**
         * Change color of message TextView
         * @param color of text
         * @return Builder
         */
        fun messageTextColor(@ColorInt color: Int): Builder {
            this.messageTextColor = color
            return this
        }

        /**
         * Change background color of CardView
         * @param color of CardView
         * @return Builder
         */
        fun background(@ColorInt color: Int): Builder {
            this.backgroundColor = color
            return this
        }

        /**
         * Padding of highlighted area in px, default value is 8dp
         * @param padding
         * @return Builder
         */
        fun paddingFocus(padding: Float): Builder {
            this.paddingFocusLeft = padding
            this.paddingFocusTop = padding
            this.paddingFocusRight = padding
            this.paddingFocusBottom = padding
            return this
        }

        /**
         * Padding of highlighted area in px, default value is 8dp
         * @param paddingLeft
         * @param paddingTop
         * @param paddingRight
         * @param paddingBottom
         * @return Builder
         */
        fun paddingFocus(paddingLeft: Float, paddingTop: Float, paddingRight: Float, paddingBottom: Float): Builder {
            this.paddingFocusLeft = paddingLeft
            this.paddingFocusTop = paddingTop
            this.paddingFocusRight = paddingRight
            this.paddingFocusBottom = paddingBottom
            return this
        }

        /**
         * Animation of bottom view
         * @param enable animation (default is true)
         * @return Builder
         */
        fun animation(enable: Boolean): Builder {
            this.isAnimationEnabled = enable
            return this
        }

        /**
         * Duration of animatiom of bottom view
         * @param duration of animation in ms
         * @return Builder
         */
        fun animationDuration(duration: Long): Builder {
            this.animationDuration = duration
            return this
        }


        private fun build(builder: Builder): GuideView {
            return GuideView(activity, builder)
        }

        /**
         * Display GuideView on top of screen
         * @return GuideView
         */
        fun show(): GuideView {
            val guideView = build(this)
            guideView.show()

            return guideView
        }
    }


}
