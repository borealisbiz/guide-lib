package com.kristijandraca.guidelib

interface GuideViewListener {
    fun onActionClick(guideView: GuideView)
}