package com.kristijandraca.guide

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.kristijandraca.guide.databinding.ActivityMainBinding
import com.kristijandraca.guidelib.GuideView
import com.kristijandraca.guidelib.GuideViewListener


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)


        binding.btTest0.setOnClickListener {
            val builder = GuideView.Builder(this)
            builder.message("Test message")
            builder.action("Next", object : GuideViewListener {
                override fun onActionClick(guideView: GuideView) {
                    guideView.dismiss()
                }

            })
            builder.show()
        }

        binding.btTest1.setOnClickListener {
            val builder = GuideView.Builder(this)
            builder.message("Focus on EditText")
            builder.target(binding.etTest, binding.ivTest)
            builder.isTargetClickable(true)
            builder.action("Next", object : GuideViewListener {
                override fun onActionClick(guideView: GuideView) {
                    guideView.dismiss()
                }

            })
            builder.show()
        }
        binding.btTest2.setOnClickListener {
            val builder = GuideView.Builder(this)
            builder.target(binding.tvTest)
            builder.paddingFocus(convertDpToPixel(8f, this))
            builder.show()
        }
        binding.btTest3.setOnClickListener {
            val dp8 = convertDpToPixel(8f, this)

            val builder = GuideView.Builder(this)
            builder.message("Test message")
            builder.target(binding.ivTest)
            builder.paddingFocus(dp8)
            builder.action("Next", object : GuideViewListener {
                override fun onActionClick(guideView: GuideView) {
                    guideView.dismiss()
                }

            })
            builder.show()
        }

    }

    private fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }
}
